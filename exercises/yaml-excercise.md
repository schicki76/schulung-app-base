# Voraussetzungen
* Editor

# Aufgabe 1
Erstelle eine YAML Datei mit dem folgendem Inhalt
* Name der Schulung
* Heutiges Datum
* Teilnehmeranzahl (fiktiv)
* Liste Teilnehmern (fiktiv)
  * Teilnehmer hat die Werte ID, Name, Job, Liste aus Fähigkeiten

 am Ende muss eine valide YAML Datei vorhanden sein (Überprüfung mit http://www.yamllint.com/)